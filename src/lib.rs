// Copyright 2018 Urs Schulz
//
// This file is part of mmap-safe.
//
// mmap-safe is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mmap-safe is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with mmap-safe.  If not, see <http://www.gnu.org/licenses/>.
//
//! This library provides a thin wrapper around the `memmap` crate in order to make file-backed
//! mapped memory safe in Rust. Although the crate tries it's best at ensuring safety, this can not
//! be fully guaranteed.
//!
//! # Safe and Unsafe usage
//! Since Linux and other Unix-Systems currently do not provide mandatory file locks (Linux does
//! but it's buggy and will be removed in the future), it is not possible to prevent parallel
//! access to a certain file. Therefore file-backed mapped memory is inherently unsafe.
//!
//! However, if you access a file only through this library and not through the [`std::fs::File`]
//! API, you are safe. This crate uses an advisory lock internally to make sure only one
//! [`MappedFile`] instance ever exists at the same time for the same file.
//!
//! Oh, and don't use it on network filesystems, I haven't tested that.
use std::fs::File;
use std::fs::OpenOptions;
use std::io;
use std::io::Seek;
use std::io::SeekFrom;
use std::marker::PhantomData;
use std::ops::Deref;
use std::ops::DerefMut;
use std::path::Path;

use fs2::FileExt;

use memmap::Mmap;
use memmap::MmapMut;

/// A file mapped to memory.
pub struct Mapping<'a> {
    mmap: Option<Mmap>,
    _lt: PhantomData<&'a ()>,
}


/// A file mutably mapped to memory.
pub struct MutMapping<'a> {
    mmap: Option<MmapMut>,
    _lt: PhantomData<&'a mut ()>,
}

impl<'a> MutMapping<'a> {
    pub fn flush(&self) -> io::Result<()> {
        self.mmap
            .as_ref()
            .map(|mm| mm.flush())
            .or(Some(Ok(())))
            .unwrap()
    }

    pub fn flush_range(&self, offset: usize, len: usize) -> io::Result<()> {
        self.mmap
            .as_ref()
            .map(|mm| mm.flush_range(offset, len))
            .or(Some(Ok(())))
            .unwrap()
    }
}

// We need this Drop in order to convince the borrow checker not to allow any new MutMapping
// instances before the old ones got fully drop()ed.
impl<'a> Drop for Mapping<'a> {
    fn drop(&mut self) {}
}

impl<'a> Drop for MutMapping<'a> {
    fn drop(&mut self) {}
}

impl<'a> Deref for Mapping<'a> {
    type Target = [u8];

    #[inline]
    fn deref(&self) -> &Self::Target {
        match self.mmap.as_ref() {
            Some(mm) => mm,
            None => &[],
        }
    }
}

impl<'a> AsRef<[u8]> for Mapping<'a> {
    #[inline]
    fn as_ref(&self) -> &[u8] {
        self.deref()
    }
}

impl<'a> Deref for MutMapping<'a> {
    type Target = [u8];

    #[inline]
    fn deref(&self) -> &Self::Target {
        match self.mmap.as_ref() {
            Some(mm) => mm,
            None => &[],
        }
    }
}

impl<'a> DerefMut for MutMapping<'a> {
    #[inline]
    fn deref_mut(&mut self) -> &mut [u8] {
        match self.mmap.as_mut() {
            Some(mm) => mm,
            None => &mut [],
        }
    }
}

impl<'a> AsRef<[u8]> for MutMapping<'a> {
    #[inline]
    fn as_ref(&self) -> &[u8] {
        self.deref()
    }
}

impl<'a> AsMut<[u8]> for MutMapping<'a> {
    #[inline]
    fn as_mut(&mut self) -> &mut [u8] {
        self.deref_mut()
    }
}

/// A thin, safe wrapper for memory-mapped files.
///
/// This wrapper ensures memory safety by only providing one mutable reference at a time and by
/// locking the file exclusively.
pub struct MappedFile {
    file: File,
    size: u64,
}

impl Drop for MappedFile {
    fn drop(&mut self) {
        self.file.unlock().unwrap();
    }
}

impl MappedFile {
    #[inline]
    pub fn open<P: AsRef<Path>>(path: P) -> io::Result<Self> {
        let file = OpenOptions::new().read(true).write(true).open(path)?;
        Self::new(file)
    }

    #[inline]
    pub fn create<P: AsRef<Path>>(path: P) -> io::Result<Self> {
        let file = OpenOptions::new()
            .create(true)
            .read(true)
            .write(true)
            .open(path)?;
        Self::new(file)
    }

    #[inline]
    pub fn new(mut file: File) -> io::Result<Self> {
        // lock file & get size
        file.try_lock_exclusive()?;
        let size = file.seek(SeekFrom::End(0))?;

        Ok(Self {
            file: file,
            size: size,
        })
    }

    /// Returns mapped memory for the file.
    #[inline]
    pub fn map(&self, offset: u64, size: usize) -> io::Result<Mapping> {
        let mmap = if size == 0 {
            None
        } else {
            Some(unsafe {
                memmap::MmapOptions::new()
                    .offset(offset)
                    .len(size)
                    .map(&self.file)?
            })
        };

        Ok(Mapping {
            mmap: mmap,
            _lt: PhantomData,
        })
    }

    /// Returns mutably-mapped memory for the file.
    #[inline]
    pub fn map_mut<'a>(&'a mut self, offset: u64, size: usize) -> io::Result<MutMapping<'a>> {
        let mmap = if size == 0 {
            None
        } else {
            Some(unsafe {
                memmap::MmapOptions::new()
                    .offset(offset)
                    .len(size)
                    .map_mut(&self.file)?
            })
        };

        Ok(MutMapping {
            mmap: mmap,
            _lt: PhantomData,
        })
    }

    /// Resizes the file. When grown, it is guaranteed that all mapped memory regions on this file
    /// stay valid. When the file gets shrunk, the behaviour of regions behind the new end of the
    /// file is undefined.
    #[inline]
    pub fn resize(&mut self, size: u64) -> io::Result<()> {
        self.file.set_len(size)?;
        self.size = size;
        Ok(())
    }

    /// Returns the current size of the file.
    #[inline]
    pub fn size(&self) -> u64 {
        self.size
    }

    /// See [`File::sync_data`]
    #[inline]
    pub fn sync_data(&mut self) -> io::Result<()> {
        self.file.sync_data()
    }

    /// See [`File::sync_all`]
    #[inline]
    pub fn sync_all(&mut self) -> io::Result<()> {
        self.file.sync_all()
    }

    /// Converts this instance into a mutable mapping.
    /// If you want to convert it back, call [`IntoMutMapping::unmap`].
    #[inline]
    pub fn into_mut_mapping(
        self,
        offset: u64,
        size: usize,
    ) -> Result<IntoMutMapping, (io::Error, Self)> {
        let mmap = if size == 0 {
            None
        } else {
            let mmap = unsafe {
                memmap::MmapOptions::new()
                    .offset(offset)
                    .len(size)
                    .map_mut(&self.file)
            };

            Some(match mmap {
                Ok(mm) => mm,
                Err(e) => return Err((e, self)),
            })
        };

        Ok(IntoMutMapping {
            mmap: mmap,
            file: Some(self),
        })
    }
}


/// Mutably mapped file obtained by [`MappedFile::into_mut_mapping()`].
pub struct IntoMutMapping {
    mmap: Option<MmapMut>,
    file: Option<MappedFile>,
}


impl IntoMutMapping {
    /// Unmaps the file from memory and returns back the [`MappedFile`] instance it originated
    /// from.
    #[inline]
    pub fn unmap(mut self) -> MappedFile {
        self.file.take().unwrap()
    }

    /// Flushes unwritten changes to disc.
    #[inline]
    pub fn flush(&self) -> io::Result<()> {
        self.mmap
            .as_ref()
            .map(|mm| mm.flush())
            .or(Some(Ok(())))
            .unwrap()
    }

    /// Flushes unwritten changes in the specific range to disc.
    #[inline]
    pub fn flush_range(&self, offset: usize, len: usize) -> io::Result<()> {
        self.mmap
            .as_ref()
            .map(|mm| mm.flush_range(offset, len))
            .or(Some(Ok(())))
            .unwrap()
    }

    /// Returns the current size of the file.
    pub fn size(&self) -> u64 {
        self.file.as_ref().unwrap().size()
    }
}

impl<'a> Deref for IntoMutMapping {
    type Target = [u8];

    #[inline]
    fn deref(&self) -> &Self::Target {
        match self.mmap.as_ref() {
            Some(mm) => mm,
            None => &[],
        }
    }
}

impl<'a> DerefMut for IntoMutMapping {
    #[inline]
    fn deref_mut(&mut self) -> &mut [u8] {
        match self.mmap.as_mut() {
            Some(mm) => mm,
            None => &mut [],
        }
    }
}

impl<'a> AsRef<[u8]> for IntoMutMapping {
    #[inline]
    fn as_ref(&self) -> &[u8] {
        self.deref()
    }
}

impl<'a> AsMut<[u8]> for IntoMutMapping {
    #[inline]
    fn as_mut(&mut self) -> &mut [u8] {
        self.deref_mut()
    }
}


#[cfg(test)]
mod tests {
    // TODO: change test file names, maybe something temporary
    use super::MappedFile;
    use std::fs::remove_file;
    use std::path::PathBuf;

    fn temp_file_name() -> PathBuf {
        use rand::Rng;

        let mut p = std::env::temp_dir();
        p.push(
            String::from("cargo-test.mmap-safe.") + &rand::thread_rng()
                .sample_iter(&rand::distributions::Alphanumeric)
                .take(12)
                .collect::<String>(),
        );
        p
    }

    #[test]
    fn only_one_instance() {
        let file = temp_file_name();
        let mut mf = MappedFile::create(&file).unwrap();
        mf.resize(10).unwrap();

        assert!(MappedFile::open(&file).is_err());
        assert!(MappedFile::create(&file).is_err());

        remove_file(file).unwrap();
    }

    #[test]
    fn write_works() {
        let file = temp_file_name();
        let mut mf = MappedFile::create(&file).unwrap();
        mf.resize(10).unwrap();

        {
            let mut mapping = mf.map_mut(0, 10).unwrap();
            mapping[0] = 0x42;
        }

        let mapping = mf.map(0, 10).unwrap();
        assert_eq!(mapping[0], 0x42);

        remove_file(file).unwrap();
    }

    #[test]
    fn multiple_immutable_mappings() {
        let file = temp_file_name();
        let mut mf = MappedFile::create(&file).unwrap();
        mf.resize(10).unwrap();

        {
            let mut mm = mf.map_mut(0, 10).unwrap();
            mm[0] = 0x42;
        }

        let mm1 = mf.map(0, 10).unwrap();
        let mm2 = mf.map(0, 10).unwrap();
        assert_eq!(mm1[0], 0x42);
        assert_eq!(mm2[0], 0x42);

        remove_file(file).unwrap();
    }

    #[test]
    fn empty_files() {
        let file = temp_file_name();
        let mut mf = MappedFile::create(&file).unwrap();

        {
            let mm = mf.map_mut(0, 0).unwrap();
            assert_eq!(mm.len(), 0);
        }

        {
            let mm = mf.map(0, 0).unwrap();
            assert_eq!(mm.len(), 0);
        }

        mf.into_mut_mapping(0, 0).map_err(|(e, _)| e).unwrap();

        remove_file(file).unwrap();
    }
}
